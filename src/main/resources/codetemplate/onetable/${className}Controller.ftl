<#include "common.ftl">  
package ${controller_package};


import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import ${model_package}.${className};
import ${service_package}.${className}Service;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.xinyuan.campus.common.constant.ApiResult;
import com.xinyuan.campus.common.ui.BootstrapTable;
import com.xinyuan.campus.common.ui.PageRequest;
import com.xinyuan.campus.common.util.BeanUtils;
import com.xinyuan.campus.core.base.BaseController;
import ${dto_package}.${className}Dto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@Api(value = "${className}管理", tags = "${className}管理")
public class ${className}Controller extends BaseController {
	
	//-------${className}管理模块地址------//
	/*** 获取所有，不分页*/
	String GET_ALL_${classNameAllUpCase}S = "v1/${classNameLowerCase}s"; 
	/*** 分页数据*/
	String GET_${classNameAllUpCase}_PAGE = "v1/${classNameLowerCase}s/page";
	/*** 获取单个信息*/
	String GET_${classNameAllUpCase} = "v1/${classNameLowerCase}s/{${classNameLowerCase}Id}";
	/*** 新增 */
	String CREATE_${classNameAllUpCase} = "v1/${classNameLowerCase}s";
	/*** 编辑*/
	String UPDATE_${classNameAllUpCase} = "v1/${classNameLowerCase}s/{${classNameLowerCase}Id}";
	/*** 删除*/
	String DELETE_${classNameAllUpCase} = "v1/${classNameLowerCase}s/{${classNameLowerCase}Id}";
	
	@Autowired
	private ${className}Service ${classNameLowerCase}Service;

	@ApiOperation("获取单个信息")
	@RequestMapping(value = GET_${classNameAllUpCase}, method = RequestMethod.GET)
	@RequiresPermissions("${classNameLowerCase}:view")
	public ApiResult get${className}(@PathVariable("${classNameLowerCase}Id") Long ${classNameLowerCase}Id) {
		${className} ${classNameLowerCase} = ${classNameLowerCase}Service.findById(${classNameLowerCase}Id);
		${className}Dto ${classNameLowerCase}Dto = null;
		if (${classNameLowerCase} != null) {
			${classNameLowerCase}Dto = converTo${className}Dto(${classNameLowerCase});
		}
		return ApiResult.success(${classNameLowerCase}Dto);
	}

	@ApiOperation("获取所有")
	@RequestMapping(value = GET_ALL_${classNameAllUpCase}S, method = RequestMethod.GET)
	@RequiresPermissions("${classNameLowerCase}:view")
	public ApiResult getAll${className}s() {
		List<${className}> ${classNameLowerCase}s = ${classNameLowerCase}Service.findAll${className}s();
		List<${className}Dto> ${classNameLowerCase}Dtos = null;
		if (${classNameLowerCase}s != null) {
			${classNameLowerCase}Dtos = ${classNameLowerCase}s.stream().map(this::converTo${className}Dto).collect(Collectors.toList());
		}
		return ApiResult.success(${classNameLowerCase}Dtos);
	}

	@ApiOperation("分页获取表格数据")
	@RequestMapping(value = GET_${classNameAllUpCase}_PAGE, method = RequestMethod.POST)
	@RequiresPermissions("${classNameLowerCase}:view")
	public ApiResult getBootstrapTable(@RequestBody PageRequest pageRequest) {
		BootstrapTable bootstrapTable = ${classNameLowerCase}Service.getBootstrapTable(pageRequest);
		return ApiResult.success(bootstrapTable);
	}

	@ApiOperation("新增")
	@RequestMapping(value = CREATE_${classNameAllUpCase}, method = RequestMethod.POST)
	@RequiresPermissions("${classNameLowerCase}:create")
	public ApiResult insert(@RequestBody ${className}Dto ${classNameLowerCase}Dto) {
		${classNameLowerCase}Dto = converTo${className}Dto(${classNameLowerCase}Service.insert(${classNameLowerCase}Dto));
		return ApiResult.success(${classNameLowerCase}Dto);
	}

	@ApiOperation("更新")
	@RequestMapping(value = UPDATE_${classNameAllUpCase}, method = RequestMethod.PUT)
	@RequiresPermissions("${classNameLowerCase}:edit")
	public ApiResult update(@RequestBody ${className}Dto ${classNameLowerCase}Dto) {
		${classNameLowerCase}Service.update(${classNameLowerCase}Dto);
		return ApiResult.success(${classNameLowerCase}Dto);
	}

	@ApiOperation("删除")
	@RequestMapping(value = DELETE_${classNameAllUpCase}, method = { RequestMethod.DELETE })
	@RequiresPermissions("${classNameLowerCase}:delete")
	public ApiResult delete(@PathVariable("${classNameLowerCase}Id") Long[] ${classNameLowerCase}Id, HttpServletRequest request) {
		${classNameLowerCase}Service.delete(${classNameLowerCase}Id);
		return ApiResult.success(${classNameLowerCase}Id);
	}
	
	/**
	 * 数据转换，提供给用户需要的数据项
	 * 
	 * @param ${classNameLowerCase}
	 * @return
	 */
	public ${className}Dto converTo${className}Dto(${className} ${classNameLowerCase}) {
		return BeanUtils.map(${classNameLowerCase},${className}Dto.class);
	}

}




