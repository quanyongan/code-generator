<#include "common.ftl">  
package ${model_package};

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

<#if table.tableRemark!="">
/**
 * ${table.tableRemark}
 */
</#if>
@TableName("${table.tableName?upper_case}")
public class ${className} extends Model<${className}> {
	<@generateFields/>
}
<#macro generateFields>
	
	<#list table.columns as column>
	<#if column.pk>
	<#if column.comment!="">
	/**
	 * ${column.comment}
	 */
	</#if>
	@TableId("${column}")
	private ${column.javaType} ${column.columnNameLowerCase};
	<#else>
	<#if column.comment!="">
	/**
	 * ${column.comment}
	 */
	</#if>
	@TableField("${column}")
	private ${column.javaType} ${column.columnNameLowerCase};
	</#if>
	</#list>
	
	<#list table.columns as column>
	<#if column.javaType == "java.util.Date">
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+08:00")
	</#if>
	public ${column.javaType} get${column.columnName}() {
		return this.${column.columnNameLowerCase};
	}
	
	public void set${column.columnName}(${column.javaType} ${column.columnNameLowerCase}) {
		this.${column.columnNameLowerCase} = ${column.columnNameLowerCase};
	}
	
	</#list>
	
	@Override
	protected Serializable pkVal() {
		return this.${table.columns[0].columnNameLowerCase};
	}
</#macro>