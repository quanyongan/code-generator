<#include "common.ftl">  
package ${serviceimpl_package};

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.mapper.Condition;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.xinyuan.common.orm.WrapperHelper;
import com.xinyuan.campus.common.constant.Constants;
import com.xinyuan.campus.common.ui.BootstrapTable;
import com.xinyuan.campus.common.ui.PageRequest;
import com.xinyuan.campus.common.util.CollectionUtils;
import com.xinyuan.campus.common.util.BeanUtils;
import com.xinyuan.campus.common.util.InstanceUtil;
import com.xinyuan.campus.common.util.RedisUtil;
import ${dto_package}.${className}Dto;
import ${dao_package}.${className}Mapper;
import ${model_package}.${className};

@Service("${classNameLowerCase}Service")
public class ${className}ServiceImpl implements ${className}Service {

	@Autowired
	private ${className}Mapper ${classNameLowerCase}Mapper;


	@Transactional
	@Override
	public ${className} insert(${className}Dto ${classNameLowerCase}Dto) {
		if (${classNameLowerCase}Dto == null) {
			return null;
		}
		${className} ${classNameLowerCase} = BeanUtils.map(${classNameLowerCase}Dto, ${className}.class);
		if (${classNameLowerCase} != null) {
			Integer insertRecord = ${classNameLowerCase}Mapper.insert(${classNameLowerCase});
			if (insertRecord == 1) {
				return ${classNameLowerCase};
			}
		}
		return null;
	}

	@Transactional
	@Override
	public Integer update(${className}Dto ${classNameLowerCase}Dto) {
		if (${classNameLowerCase}Dto == null) {
			return null;
		}
		${className} ${classNameLowerCase} = BeanUtils.map(${classNameLowerCase}Dto, ${className}.class);
		if (${classNameLowerCase} != null) {
			return ${classNameLowerCase}Mapper.updateById(${classNameLowerCase});
		}
		return null;
	}

	@Transactional
	@Override
	public Integer delete(Long... ${classNameLowerCase}Ids) {
		return ${classNameLowerCase}Mapper.deleteBatchIds(Arrays.asList(${classNameLowerCase}Ids));
	}
	
	@Transactional(readOnly = true)
	@Override
	public List<${className}> findAll${className}s() {
		return ${classNameLowerCase}Mapper.selectList(null);
	}
	
	@Transactional(readOnly = true)
	@Override
	public ${className} findById(Long ${classNameLowerCase}Id) {
		return ${classNameLowerCase}Mapper.selectById(${classNameLowerCase}Id);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public BootstrapTable getBootstrapTable(PageRequest pageRequest) {
		Page<${className}> page = new Page<${className}>(pageRequest.getCurrent(), pageRequest.getLimit());
		EntityWrapper<${className}> ew = WrapperHelper.buildWrapper(${className}.class, pageRequest.getCondition(), pageRequest.getOrderBy());
		BootstrapTable bootstrapTable = BootstrapTable.instance();
		List<${className}> pageDatas = ${classNameLowerCase}Mapper.selectPage(page, ew);
		if (pageDatas != null) {
			List<${className}Dto> rows = BeanUtils.mapList(pageDatas, ${className}Dto.class);
			bootstrapTable.setRows(rows);
		}
		bootstrapTable.setTotal(page.getTotal());
		bootstrapTable.setRecords(page.getSize());
		return bootstrapTable;
	}
}