<#include "common.ftl"> 
package ${dao_package};

import org.apache.ibatis.annotations.Param;

import ${model_package}.${className};

import com.xinyuan.campus.core.base.BaseMapper;

public interface ${className}Mapper extends BaseMapper<${className}> {

}
