<#include "common.ftl">  
package ${dto_package};

import java.io.Serializable;

<#if table.tableRemark!="">
/**
 * ${table.tableRemark}
 */
</#if>
public class ${className}Dto implements Serializable {
	<@generateFields/>
}
<#macro generateFields>
	
	<#list table.columns as column>
	<#if column.pk>
	<#if column.comment!="">
	/**
	 * ${column.comment}
	 */
	</#if>
	private ${column.javaType} ${column.columnNameLowerCase};
	<#else>
	<#if column.comment!="">
	/**
	 * ${column.comment}
	 */
	</#if>
	private ${column.javaType} ${column.columnNameLowerCase};
	</#if>
	</#list>
	
	<#list table.columns as column>
	<#if column.javaType == "java.util.Date">
	</#if>
	public ${column.javaType} get${column.columnName}() {
		return this.${column.columnNameLowerCase};
	}
	
	public void set${column.columnName}(${column.javaType} ${column.columnNameLowerCase}) {
		this.${column.columnNameLowerCase} = ${column.columnNameLowerCase};
	}
	
	</#list>
</#macro>