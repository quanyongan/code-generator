<#include "common.ftl">  
package ${service_package};

import java.util.List;

import com.xinyuan.campus.common.ui.BootstrapTable;
import com.xinyuan.campus.common.ui.PageRequest;
import ${dto_package}.${className}Dto;
import ${model_package}.${className};

public interface ${className}Service {
	

	/**
	 * 新增
	 * 
	 * @param ${classNameLowerCase}Dto
	 *            
	 * @return
	 */
	${className} insert(${className}Dto ${classNameLowerCase}Dto);

	/**
	 * 编辑
	 * 
	 * @param ${classNameLowerCase}Dto
	 *            
	 * @return
	 */
	Integer update(${className}Dto ${classNameLowerCase}Dto);

	/**
	 * 删除
	 * 
	 * @param ${classNameLowerCase}Ids
	 *            
	 * @return
	 */
	Integer delete(Long... ${classNameLowerCase}Ids);

	/**
	 * 获取所有的列表
	 * 
	 * @return
	 */
	List<${className}> findAll${className}s();
	
	/**
	 * 获取单个详情
	 * 
	 * @return
	 */
	${className} findById(Long ${classNameLowerCase}Id);

	/**
	 * <p>
	 * 获取BootstrapTable需要的数据格式
	 * </p>
	 *
	 * @param pageRequest
	 *            分页对象
	 * @return
	 */
	BootstrapTable getBootstrapTable(PageRequest pageRequest);
	
}
